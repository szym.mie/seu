import { GLRenderer } from "./modules/gl/glrenderer.js";

const CANVAS = document.getElementById("canvas");

const renderer = new GLRenderer(CANVAS);
renderer.render();