varying highp vec2 v_texture_coord;

uniform sampler2D u_diff_texture;

void 
main ()
{
    gl_FragColor = texture2D(u_diff_texture, v_texture_coord);
}