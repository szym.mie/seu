attribute vec4 a_vertex_pos;
attribute vec2 a_texture_coord;

uniform mat4 u_projection_mat;
uniform mat4 u_modelview_mat;

varying highp vec2 v_texture_coord;

void 
main ()
{
    gl_Position = u_projection_mat*u_modelview_mat*a_vertex_pos;
    v_texture_coord = a_texture_coord;
}