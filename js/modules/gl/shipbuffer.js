import { GLBuffer } from "./glbuffer.js";

class ShipBuffer extends GLBuffer
{
    constructor (gl)
    {
        super(gl, gl.ARRAY_BUFFER, gl.STATIC_DRAW);
        this.data(this.vertices);
    }

    vertices = new Float32Array([
        -1.0,  1.0, 0,
        1.0,  1.0, 0,
        -1.0, -1.0, 0,
        1.0, -1.0, 0.5
    ]);

    tex_coords = new Float32Array([
        0.0, 1.0,
        1.0, 1.0,
        0.0, 0.0,
        1.0, 0.0
    ]);
}

export { ShipBuffer };