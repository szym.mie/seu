class Matrix4
{
    constructor ()
    {
        this.mat = null;
        this.identity();
    }

    identity ()
    {
        this.mat = new Float32Array(16).fill(0).map((_, i) => i % 5 ? 0 : 1);
    }
    
    zero ()
    {
        this.mat.fill(0);
    }

    multiply (mat)
    {
        // fast dot product.
        const dot = (a, b) => 
        {
            let sum = 0;
            for (let i = 0; i < 4; i++) sum+=a[i]*b[i];
            return sum;
        };

        // a reference
        const m = this.mat;

        // rows of this matrix.
        const row0 = [m[0], m[1], m[2], m[3]];
        const row1 = [m[4], m[5], m[6], m[7]];
        const row2 = [m[8], m[9], m[10], m[11]];
        const row3 = [m[12], m[13], m[14], m[15]];

        // columns of argument matrix.
        const col0 = [m[0], m[4], m[8], m[12]];
        const col1 = [m[1], m[5], m[9], m[13]];
        const col2 = [m[2], m[6], m[10], m[14]];
        const col3 = [m[3], m[7], m[11], m[15]];

        m[0] = dot(row0, col0);
        m[1] = dot(row0, col1);
        m[2] = dot(row0, col2);
        m[3] = dot(row0, col3);
        
        m[4] = dot(row1, col0);
        m[5] = dot(row1, col1);
        m[6] = dot(row1, col2);
        m[7] = dot(row1, col3);
        
        m[8] = dot(row2, col0);
        m[9] = dot(row2, col1);
        m[10] = dot(row2, col2);
        m[11] = dot(row2, col3);

        m[12] = dot(row3, col0);
        m[13] = dot(row3, col1);
        m[14] = dot(row3, col2);
        m[15] = dot(row3, col3);
    }

    translate (x, y, z)
    {
        this.mat[12] += x;
        this.mat[13] += y;
        this.mat[14] += z;
    }

    fromAxisAngle (x, y, z, a)
    {
        const c = Math.cos(a);
        const s = Math.sin(a);
        const t = 1 - c;
        
        const txy = t*x*y;
        const tyz = t*y*z;
        const tzx = t*z*x;

        const xs = x*s;
        const ys = y*s;
        const zs = z*s;

        this.mat[0] = t*x*x + c;
        this.mat[1] = txy - zs;
        this.mat[2] = tzx + ys;
        this.mat[4] = txy + zs;
        this.mat[5] = t*y*y + c;
        this.mat[6] = tyz - xs;
        this.mat[8] = tzx - ys;
        this.mat[9] = tyz + xs;
        this.mat[10] = t*z*z + c;
    }

    perspective (fov, asp, near, far)
    {
        const s = Math.tan(fov * .5) * near;
        const r = asp * s;
        const l = -r;
        const t = s;
        const b = -t;

        this.frustum(b, t, l, r, near, far);
    }

    frustum (b, t, l, r, n, f)
    {
        const dn = 2*n;
        const sfn = f - n;

        this.zero();

        this.mat[0] = dn / (r - l);
        this.mat[5] = dn / (t - b);
        this.mat[10] = -(f + n) / sfn;
        this.mat[11] = -1;
        this.mat[14] = -dn * f / sfn;
    }
}

export { Matrix4 };