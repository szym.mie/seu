class GLBuffer
{
    constructor (gl, type, draw, components=3, stride=0, offset=0)
    {
        this.gl = gl;
        this.type = type;
        this.draw = draw;
        this.buffer = null;
        
        this.components = components;
        this.stride = stride;
        this.offset = offset;
    }

    data (data)
    {
        const b = this.gl.createBuffer();
        this.gl.bindBuffer(this.type, b);

        this.gl.bufferData(this.type, data, this.draw);

        this.buffer = b;
    }
}

export { GLBuffer };