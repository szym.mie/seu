class GLShader
{
    constructor (gl) 
    {
        this.gl = gl;
        this.vertex = null;
        this.fragment = null;
        this.program = null;
    }

    compile (src, type)
    {
        let s = this.gl.createShader(type);
        this.gl.shaderSource(s, src);
        this.gl.compileShader(s);
        if (!this.gl.getShaderParameter(s, this.gl.COMPILE_STATUS))
        {
            const i = this.gl.getShaderInfoLog(s);
            this.gl.deleteShader(s);
            s = null;
            throw Error(`unable to compile shader: ${type === this.gl.VERTEX_SHADER ? "vertex" : "fragment"}.\nlog: ${i}`);
        }

        switch (type)
        {
            case this.gl.VERTEX_SHADER:
                this.vertex = s;
                break;
            case this.gl.FRAGMENT_SHADER:
                this.fragment = s;
                break;
            default:
                break;
        }
    }

    link ()
    {
        let p = this.gl.createProgram();
        this.gl.attachShader(p, this.vertex);
        this.gl.attachShader(p, this.fragment);
        this.gl.linkProgram(p);

        if (!this.gl.getProgramParameter(p, this.gl.LINK_STATUS))
        {
            const i = this.gl.getProgramInfoLog(p);
            p = null;
            throw Error(`unable to link program: ${type}.\nlog: ${i}`);
        }
        this.program = p;
    }

    use ()
    {
        // if you use textures in this shader, bind them here (max. 8);
    }
}

export { GLShader };