import { GLShader } from "./glshader.js";

class ShipShader extends GLShader
{
    constructor (gl, diff_map, norm_map, glos_map)
    {
        super(gl);

        this.diff_map = diff_map;
        this.norm_map = norm_map;
        this.glos_map = glos_map;
        this.compile(this.vertex_src, this.gl.VERTEX_SHADER);
        this.compile(this.fragment_src, this.gl.FRAGMENT_SHADER);
        this.link();

        this.program_info = {
            attrib: [
                this.gl.getAttribLocation(this.program, "a_vertex_pos"),
                this.gl.getAttribLocation(this.program, "a_texture_coord")
            ],
            uniform: [
                this.gl.getUniformLocation(this.program, "u_projection_mat"),
                this.gl.getUniformLocation(this.program, "u_modelview_mat"),
                this.gl.getUniformLocation(this.program, "u_diff_texture"),
                this.gl.getUniformLocation(this.program, "u_norm_texture"),
                this.gl.getUniformLocation(this.program, "u_glos_texture")
            ]
        };
    }

    use ()
    {
        this.gl.activeTexture(this.gl.TEXTURE0);
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.diff_map.texture);
        this.gl.uniform1i(this.program_info.uniform[2], 0);

        // future use.
        // this.gl.activeTexture(this.gl.TEXTURE1);
        // this.gl.bindTexture(this.gl.TEXTURE_2D, this.norm_map);
        // this.gl.uniform1i(this.program_info.uniform[3], 1);

        // this.gl.activeTexture(this.gl.TEXTURE2);
        // this.gl.bindTexture(this.gl.TEXTURE_2D, this.glos_map);
        // this.gl.uniform1i(this.program_info.uniform[4], 2);
    }

    vertex_src = `
    attribute vec4 a_vertex_pos;
    attribute vec2 a_texture_coord;
    
    uniform mat4 u_projection_mat;
    uniform mat4 u_modelview_mat;
    
    varying highp vec2 v_texture_coord;
    
    void 
    main ()
    {
        gl_Position = u_projection_mat*u_modelview_mat*a_vertex_pos;
        v_texture_coord = a_texture_coord;
    }
    `;

    fragment_src = `
    varying highp vec2 v_texture_coord;

    uniform sampler2D u_diff_texture;
    
    void 
    main ()
    {
        gl_FragColor = texture2D(u_diff_texture, v_texture_coord);
        //gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
    `;
}

export { ShipShader };