class GLModel
{
    constructor (gl, shader, vertex_pos)
    {
        this.gl = gl;
        this.shader = shader;
        this.vertex_pos = vertex_pos;
        this.count = 0;
    }

    use ()
    {
        this.shader.use();

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertex_pos.buffer);
        this.gl.vertexAttribPointer(
            this.shader.program_info.attrib[0], 
            this.vertex_pos.components, 
            this.gl.FLOAT,
            false,
            this.vertex_pos.stride,
            this.vertex_pos.offset);

        this.gl.enableVertexAttribArray(this.shader.program_info.attrib[0]);
    }
}


export { GLModel };