import { Matrix4 } from "./glmatrix.js";
import { ShipModel } from "./shipmodel.js";

class GLRenderer 
{
    constructor (can) 
    {
        this.can = can;
        this.gl = this.can.getContext("webgl");
        if (!this.gl) throw Error("unable to init webgl.");

        this.fov = 0.707;
        this.asp = this.can.clientWidth / this.can.clientHeight;
        this.z_near = 0.1;
        this.z_far = 100.0;

        this.uniform = [
            new Matrix4(),
            new Matrix4()
        ];

        this.uniform[0].perspective(this.fov, this.asp, this.z_near, this.z_far);
        this.uniform[1].translate(0, 0, -6);
        // this.uniform[1].fromAxisAngle(0.707106, 0.707106, 0, 1.0);

        this.T = 0;

        this.ship_model = new ShipModel(this.gl);
    }

    model (model)
    {
        this.gl.useProgram(model.shader.program);
        for (let j = 0; j < this.uniform.length; j++)
        {
            this.gl.uniformMatrix4fv(
                model.shader.program_info.uniform[j],
                false,
                this.uniform[j].mat
            );
        }

        model.use();
    }

    render ()
    {
        this.gl.clearColor(0, 0, 0, 1);
        this.gl.clearDepth(1.0);

        this.gl.enable(this.gl.DEPTH_TEST);
        this.gl.depthFunc(this.gl.LEQUAL);

        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

        this.model(this.ship_model);

        this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, this.ship_model.count);

        this.uniform[1].fromAxisAngle(0.707106, 0.707106, 0, this.T/100);

        this.T++;
        requestAnimationFrame(() => this.render());
    }

    viewport (w, h)
    {
        this.can.width = w;
        this.can.height = h;
        this.gl.viewport(
            0, 0, 
            this.gl.drawingBufferWidth, this.gl.drawingBufferHeight
            );
    }
}

export { GLRenderer };