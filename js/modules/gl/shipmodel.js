import { GLBuffer } from "./glbuffer.js";
import { GLModel } from "./glmodel.js";
import { GLTexture } from "./gltexture.js";
import { ShipShader } from "./shipshader.js";

class ShipModel extends GLModel
{
    constructor (gl)
    {
        const vp = new GLBuffer(gl, gl.ARRAY_BUFFER, gl.STATIC_DRAW);
        const tc = new GLBuffer(gl, gl.ARRAY_BUFFER, gl.STATIC_DRAW, 2);

        const dt = new GLTexture(gl, "/tex/board.jpg");
        dt.load();

        super(
            gl, 
            new ShipShader(gl, dt, null, null),
            vp
            );

        this.tex_coords = tc;

        this.vertex_pos.data(this.model_vertex_pos);
        this.tex_coords.data(this.model_tex_coords);
        this.count = 4;
    }

    use ()
    {
        super.use();

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tex_coords.buffer);
        this.gl.vertexAttribPointer(
            this.shader.program_info.attrib[1], 
            this.tex_coords.components, 
            this.gl.FLOAT,
            false,
            this.tex_coords.stride,
            this.tex_coords.offset);

        this.gl.enableVertexAttribArray(this.shader.program_info.attrib[1]);
    }

    model_vertex_pos = new Float32Array([
        -1.0,  -1.0, 0.0,
        1.0,  -1.0, 0.0,
        -1.0, 1.0, 0.0,
        1.0, 1.0, 0.0
    ]);

    model_tex_coords = new Float32Array([
        0.0, 0.0,
        1.0, 0.0,
        0.0, 1.0,
        1.0, 1.0
    ]);
}

export { ShipModel };