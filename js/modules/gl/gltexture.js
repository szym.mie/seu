class GLTexture 
{
    constructor (gl, src) 
    {
        this.gl = gl;
        this.src = src;
        this.texture = null;
    }

    load ()
    {
        this.texture = this.gl.createTexture();
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);

        function _onload (img) {
            console.log(this, img);
            this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
            this.gl.texImage2D(this.gl.TEXTURE_2D, 0,
                this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, img);
            this.gl.generateMipmap(this.gl.TEXTURE_2D);
            // this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
        }

        const w = 1;
        const h = 1;
        const pixel = new Uint8Array([16, 16, 16, 255]);
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0,
            this.gl.RGBA, w, h, 0, 
            this.gl.RGBA, this.gl.UNSIGNED_BYTE, pixel);

        const image = new Image();
        image.crossOrigin = "";
        image.src = this.src;
        image.onload = _onload.call(this, image);
    }
}

export { GLTexture };